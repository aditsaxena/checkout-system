class Item

  attr_accessor :item_id, :name, :price

  def initialize(item_id: item_id, name: name, price: price)
    @item_id, @name, @price = item_id, name, price
  end

end
