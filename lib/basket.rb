class Basket

  include Promotionable

  attr_reader :current_total
  attr_reader :items

  def initialize(promotional_rules = [])
    super()
    add_observer promotional_rules

    @current_total = 0
    @items = []
  end

  def add(item)
    @current_total += item.price
    @items << item
    notify_observers(:update_item)
  end

  def total
    notify_observers(:calc_total)
    @current_total
  end

end
