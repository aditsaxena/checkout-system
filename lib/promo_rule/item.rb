module PromoRule

  class Item

    def initialize(item_id, min_items, discount)
      @n_items = 0
      @item_id, @min_items, @discount = item_id, min_items, discount
    end

    def update(action_name, basket)
      return unless action_name == :update_item

      n_current_items = calc_current_items(basket) - @n_items
      return unless n_current_items >= @min_items

      fin_discount = 0
      fin_discount =   n_current_items * @discount if n_current_items > @n_items
      fin_discount = - n_current_items * @discount if n_current_items < @n_items

      if fin_discount != 0
        @n_items = n_current_items

        basket.instance_variable_set :"@current_total", (basket.current_total - fin_discount).round(2)
      end

    end

    private

      def calc_current_items(basket)
        basket.items.map(&:item_id).select { |item_id| item_id == @item_id }.length
      end

  end

end