module PromoRule

  class TotalPercentage

    def initialize(discount, min_total)
      @discount, @min_total = discount, min_total
    end

    def update(action_name, basket)
      return unless action_name == :calc_total

      return unless basket.current_total > @min_total
      discount = + (basket.current_total / 100.0) * @discount
      
      basket.instance_variable_set :"@current_total", (basket.current_total - discount).round(2)
    end

  end

end