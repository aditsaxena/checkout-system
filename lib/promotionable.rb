module Promotionable

  def initialize
    @observers = []
  end

  def add_observer(observers = [])
    observers.each { |observer| @observers << observer }
  end

  def delete_observer(observers = [])
    @observers.each { |observer| @observers.delete(observer) }
  end

  private

    def notify_observers(action_name)
      @observers.each { |observer| observer.update(action_name, self) }
    end

end