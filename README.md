# Promotion System for Online store

Offer promotions as an incentive for our customers to purchase items.

    Given the following products exist:
      | id  | name                   | price  |
      | 001 | Travel Card Holder     | £9.25  |
      | 002 | Personalized cufflinks | £45.00 |
      | 003 | Kids T-shirt           | £19.95 |

If you spend over £60, then you get 10% off of your purchase.
If you buy two or more travel card holders then the price drops to £ 8.50.

    Expected usage:
      basket = Basket.new promotional_rules
      basket.add @item_001
      basket.add @item_002
      basket.add @item_001
      basket.add @item_003

      price = basket.total

Sample expected results:

    basket: 001,002,003
    total price expected: £66.78

    basket: 001,003,001
    total price expected: £36.95

    basket: 001,002,001,003
    total price expected: £73.76

-------------


    Usage:

