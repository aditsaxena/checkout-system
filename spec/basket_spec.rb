require 'spec_helper'

describe Basket do

  let(:promotional_rules) {
    [
      PromoRule::TotalPercentage.new(10, 60),
      PromoRule::Item.new('001', 2, 0.75),
    ]
  }

  before do
    @item_001 = Item.new item_id: '001', name: 'Travel Card Holder', price: 9.25
    @item_002 = Item.new item_id: '002', name: 'Personalized cufflinks', price: 45.00
    @item_003 = Item.new item_id: '003', name: 'Kids T-shirt', price: 19.95
  end

  it "Tests Promotional Rules" do
    basket = Basket.new promotional_rules
    basket.add @item_001
    basket.add @item_002
    basket.add @item_003

    price = basket.total

    expect(price).to eq(66.78)
  end

  it "More than one Travel card" do
    basket = Basket.new promotional_rules
    basket.add @item_001
    basket.add @item_003
    basket.add @item_001

    price = basket.total

    expect(price).to eq(36.95)
  end

  it "per item and total discount together" do
    basket = Basket.new promotional_rules
    basket.add @item_001
    basket.add @item_002
    basket.add @item_001
    basket.add @item_003

    price = basket.total

    expect(price).to eq(73.76)
  end

end
