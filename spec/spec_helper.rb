require './lib/environment'

require 'awesome_print'
require 'pry'

Dir["./spec/support/*.rb"].each {|file| require file }

RSpec.configure do |config|

  config.filter_run :focus
  config.run_all_when_everything_filtered = true

  # if config.files_to_run.one?
    config.default_formatter = 'doc'
  # end

  config.before(:each) do
    # stub_request(:any, /website.com/).to_rack WebsiteDotComFakeweb
  end

  config.profile_examples = 10 # Print the 10 slowest examples

  config.order = :random # rspec --seed 1234
  Kernel.srand config.seed

  config.expect_with :rspec do |expectations|
    expectations.syntax = :expect
  end

  config.mock_with :rspec do |mocks|
    mocks.syntax = :expect

    mocks.verify_partial_doubles = true
  end

end
